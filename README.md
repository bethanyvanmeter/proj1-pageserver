# README #

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

The following functionality is in pageserver.py. 
(a) If URL ends with `name.html` or `name.css` (i.e., if `path/to/name.html` is in document path (from DOCROOT)), the content of `name.html` or `name.css` is sent with proper http response. 
(b) If `name.html` is not in current directory a 404 (not found) response is sent. 
(c) If a page starts with one of the symbols(~ // ..), a 403 forbidden error response is sent. For example, `url=localhost:5000/..name.html` or `/~name.html` would give 403 forbidden error.
  
  ```
  ## Author: Bethany Van Meter, bvanmet2@uoregon.edu ##
  ```

  To run:

  `git clone https://bethanyvanmeter@bitbucket.org/bethanyvanmeter/proj1-pageserver.git <targetDirectory>`

  `cd <targetDirectory>`

  `make run` or `make start`

  -> test it with a browser now, while the server is running in a background process

  `make stop`